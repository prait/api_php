<?php

	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST");
	header('Content-Type: application/json');
	//header('WWW-Authenticate: Basic realm="https://www.siamcityinsurance.com",charset="UTF-8" '); 
	//header("HTTP/1.0 401 Unauthorized");
		
	if (trim($_SERVER['PHP_AUTH_USER'])=="Fr@nk2Gi" and trim($_SERVER['PHP_AUTH_PW']) == "FwdGi2@20") {

		include_once("../include/misc.php");
		$conn = connect_db();

			$data = json_decode(file_get_contents('php://input'), true);
				
			foreach ($data as $pol) {
				$tempOrderNo			=	$pol["tempOrderNo"];
				$tempAgentLicense		=	$pol["tempAgentLicense"]; 
				$tempIntmId				=	$pol["tempIntmId"]; 
				$tempTitleLifeNm		= 	$pol["tempTitleLifeNm"]; 
				$tempFirstLifeNm		=	$pol["tempFirstLifeNm"]; 
				$tempLastLifeNm		=	$pol["tempLastLifeNm"]; 
				$tempLifeIc				=	$pol["tempLifeIc"]; 
				$tempLifeBirth			=	$pol["tempLifeBirth"]; 
				$tempLifeAddrNo		=	$pol["tempLifeAddrNo"];
				$tempLifeBuilding 		=	$pol["tempLifeBuilding"]; 
				$tempLifeTambol		= 	$pol["tempLifeTambol"]; 
				$tempLifeAmphur		=	$pol["tempLifeAmphur"]; 
				$tempLifeChangewat	=	$pol["tempLifeChangewat"]; 
				$tempLifePostcode		=	$pol["tempLifePostcode"]; 
				$tempLifeMobile			=	$pol["tempLifeMobile"]; 
				$tempEmail				=	$pol["tempEmail"]; 
				$tempFormula			=	$pol["tempFormula"]; 
				//$tempNoPerson		=	$pol["tempNoPerson"]; 
				$tempEffectDt			=	$pol["tempEffectDt"]; 
				$tempExpiryDt			=	$pol["tempExpiryDt"]; 
				$tempPremium			= 	$pol["tempPremium"]; 
				//$tempQuotationNo	=  $pol["tempQuotationNo"]; 
				//$tempRefNo1			=  $pol["tempRefNo1"]; 
				//$tempRefNo2			=  $pol["tempRefNo2"]; 
				//$tempSubclass		=	$pol["tempSubclass"]; 
				//$tempSumInsured		=  $pol["tempSumInsured"]; 
				$tempTitleBeneNm		=  $pol["tempTitleBeneNm"]; 
				$tempFirstBeneNm		=  $pol["tempFirstBeneNm"]; 
				$tempLastBeneNm		=  $pol["tempLastBeneNm"]; 
				$tempRelation			=  $pol["tempRelation"]; 
				$tempBeneIc				= 	$pol["tempBeneIc"]; 
				//$tempBeneRate		=  $pol["tempBeneRate"]; 
				$tempStatus				=  $pol["tempStatus"];

				$snote .= 	"tempOrderNo = ".$tempOrderNo."\n";
				$snote .= 	"tempAgentLicense = ".$tempAgentLicense."\n";
				$snote .= 	"tempIntmId = ".$tempIntmId."\n";
				$snote .= 	"tempTitleLifeNm = ".$tempTitleLifeNm."\n";
				$snote .= 	"tempFirstLifeNm = ".$tempFirstLifeNm."\n";
				$snote .= 	"tempLastLifeNm = ".$tempLastLifeNm."\n";
				$snote .= 	"tempLifeIc = ".$tempLifeIc."\n";
				$snote .= 	"tempLifeBirth = ".$tempLifeBirth."\n";		
				$snote .= 	"tempLifeAddrNo = ".$tempLifeAddrNo."\n";
				$snote .= 	"tempLifeBuilding = ".$tempLifeBuilding."\n";
				$snote .= 	"tempLifeTambol = ".$tempLifeTambol."\n";
				$snote .= 	"tempLifeAmphur = ".$tempLifeAmphur."\n";
				$snote .= 	"tempLifeChangewat = ".$tempLifeChangewat."\n";
				$snote .= 	"tempLifePostcode = ".$tempLifePostcode."\n";
				$snote .= 	"tempLifeMobile = ".$tempLifeMobile."\n";
				$snote .= 	"tempEmail = ".$tempEmail."\n";
				$snote .= 	"tempFormula = ".$tempFormula."\n";	
				$snote .= 	"tempEffectDt = ".$tempEffectDt."\n";	
				$snote .= 	"tempExpiryDt = ".$tempExpiryDt."\n";	
				$snote .= 	"tempPremium = ".$tempPremium."\n";	
				$snote .= 	"tempSumInsured = ".$tempSumInsured."\n";	
				$snote .= 	"tempTitleBeneNm = ".$tempTitleBeneNm."\n";	
				$snote .= 	"tempFirstBeneNm = ".$tempFirstBeneNm."\n";	
				$snote .= 	"tempLastBeneNm = ".$tempLastBeneNm."\n";	
				$snote .= 	"tempRelation = ".$tempRelation."\n";	
				$snote .= 	"tempBeneIc = ".$tempBeneIc."\n";	
				$snote .= 	"tempStatus = ".$tempStatus."\n";	
					
				$COMDATE 	= $tempEffectDt;
				$COMDATE	= substr($tempEffectDt, -4)."-".substr($tempEffectDt,3,2)."-".substr($tempEffectDt,0,2);
				$EXPDATE 	= $tempExpiryDt;
				$EXPDATE 	= substr($tempExpiryDt, -4)."-".substr($tempExpiryDt,3,2)."-".substr($tempExpiryDt,0,2);
				
				$date_regex = '/^(0[1-9]|[12][0-9]|3[01])[\-\/.](0[1-9]|1[012])[\-\/.](19|20)\d\d$/';	
	
				if ($tempStatus <> "SUCCESS") {
					$return  = array(
					"tempStatus"=>"FAIL",
					"Message"=>array("tempOrderNo"=>$tempOrderNo,
													"ErrorCode"=>"1BUS03",
													"ErrorMessage"=>"Please check validate again",
												));
					$res		= json_encode($return);

				} else if ((!preg_match($date_regex,$tempEffectDt)) || (!preg_match($date_regex,$tempExpiryDt))) {
					$return  = array(
						"tempStatus"=>"FAIL",
						"Message"=>array("tempOrderNo"=>$tempOrderNo,
														"ErrorCode"=>"1SYS01",
														"ErrorMessage"=>"Must be format DD/MM/YYYY",
													));
					$res		= json_encode($return);			

				} else if(strtotime($COMDATE) < strtotime(date("Y-m-d"))) {
					$return  = array(
						"tempStatus"=>"FAIL",
						"Message"=>array("tempOrderNo"=>$tempOrderNo,
														"ErrorCode"=>"1SYS02",
														"ErrorMessage"=>"tempEffectDt back date",
													));
					$res		= json_encode($return);

				} else if(strtotime($COMDATE) >= strtotime($EXPDATE)) {
					$return  = array(
						"tempStatus"=>"FAIL",
						"Message"=>array("tempOrderNo"=>$tempOrderNo,
														"ErrorCode"=>"1SYS03",
														"ErrorMessage"=>"tempEffectDt must less than tempExpiryDt",
													));
					$res		= json_encode($return);
				
				} else if(($tempFormula <> "HP001") && ($tempFormula <> "HP002") && ($tempFormula <> "HP003") && ($tempFormula <> "HP004") && ($tempFormula <> "HP005") && ($tempFormula <> "HP006")){
					$return  = array(
						"tempStatus"=>"FAIL",
						"Message"=>array("tempOrderNo"=>$tempOrderNo,
														"ErrorCode"=>"1SYS04",
														"ErrorMessage"=>"tempFormula incorrect",
													));
					$res		= json_encode($return);

				} else {

						$sql = "SELECT * FROM MAS_APPLICATION";
						$sql .= " WHERE tempIcCard = '" .$tempIcCard ."'";
						$sql .= " AND tempSubclass = 'HPO'";
						$sql .= " AND POLICY_EXPDATE > '" .$comdate ."'";
						$sql .= " AND tempStatus <> 0 ";
						$result = mysql_query($sql);
						if ($rs = mysql_fetch_array($result)) {
							$policy_temp = $rs["POLICY_ID"];
							$expdate_temp = $rs["POLICY_EXPDATE"];
							$return  = array(
								"tempStatus"=>"FAIL",
								"Message"=>array("tempOrderNo"=>$tempOrderNo,
																"ErrorCode"=>"1BUS01",
																"ErrorMessage"=>"max 1 policies/person",
															));
							$res		= json_encode($return);
							
							$sql = "SELECT SENT_NO FROM TXN_LOG_SOAP";
							$sql .= " WHERE POLICY_ID = '".$tempOrderNo."'";
							$sql .= " ORDER BY SENT_NO DESC";
							$result = mysql_query($sql);
							
							if ($rs = mysql_fetch_array($result)) {
								$sent_no = strval(intval($rs["SENT_NO"]) + 1);
							} else {
								$sent_no = '1';
							}
					
							$sql = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE,AGENT_ID) VALUES";
							$sql .= " ('".$tempOrderNo."', ". $sent_no .", 'Validate_Health', now()";
							$sql .= ", '".getenv("REMOTE_ADDR")."', '1BUS01', 'FAIL', '" . $snote ."','')";
							mysql_unbuffered_query($sql);

						} else {
	
							//-------- RUNNING POLICY ------------//
							$database		= "scil_test";
							$policy_type	=	"HPO";
							$BRANCH_ID	= "HO1" ;
							//$tempSumInsured = "100000";
																						
							$sql = "SELECT * FROM $database.MAS_RUNNING";
							$sql .= " WHERE POLICY_GROUP = '".$policy_type."'";
							$sql .= " AND BRANCH_ID ='".$BRANCH_ID."'";
							$sql .= " AND YEARS = '".date("Y")."'";
							$result = mysql_query($sql);
							if ($rs = mysql_fetch_array($result)) {
								$running = $rs["RUNNING_NUMBER"] + 1;
																								
								$policy_id = $BRANCH_ID."-".date("y")."-P".$policy_type."-". substr("000000".strval($running), -6);
																							
								$sql = "UPDATE $database.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
								$sql .= " WHERE POLICY_GROUP = '".$policy_type."'";
								$sql .= " AND BRANCH_ID = '".$BRANCH_ID."'";
								$sql .= " AND YEARS = '".date("Y")."'";
								mysql_unbuffered_query($sql);
							} else {
								$policy_id = $BRANCH_ID."-".date("y")."-P".$policy_type."-000001";
															
								$sql = "INSERT INTO $database.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','HO1','".$policy_type."','1')";
								mysql_unbuffered_query($sql);
							}

							$sql = "SELECT * FROM $database.MAS_RUNNING";
							$sql .= " WHERE POLICY_GROUP = 'INVOICE_PA'";
							$sql .= " AND YEARS = '".date("Y")."'";
							$result = mysql_query($sql);
							if ($rs = mysql_fetch_array($result)) {
								$running = $rs["RUNNING_NUMBER"] + 1;
															
								$receipt_id = "MPA". date("y").substr("000000".strval($running), -6);
															
								$sql = "UPDATE $database.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
								$sql .= " WHERE POLICY_GROUP = 'INVOICE_PA'";
								$sql .= " AND YEARS = '".date("Y")."'";
								mysql_unbuffered_query($sql);
							} else {
								$receipt_id = "MPA". date("y")."000001";
															
								$sql = "INSERT INTO $database.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','HO1','INVOICE_PA','1')";
								mysql_unbuffered_query($sql);
							}
			
							//-------- INSERT MAS POLICY SOAP ------------//
							/*$sql_formula 		= "SELECT * FROM scil_webservice.mas_package_pa WHERE PACKAGE_ID='$tempPackage' ";
							$query_formula 	= mysql_query($sql_formula) or die (mysql_error());
							$row_formula	= mysql_fetch_array($query_formula);																	
							$tempFormula	=	$row_formula['FORMULA'];*/
							
							$tempCommGrp	= "ST_OV";
							$tempYear			=	date('Y');
							//$tmp01			= 	date('d/m/Y');
							$tempDayendDt	= 	date('Y-m-d H:i:s');
							$tempInformer	=  $tempTitleLifeNm.$tempFirstLifeNm." ".$tempLastLifeNm;
							$tempMainClass	= 	"P";
							$tempMajorSrc  = 'AG';
							$tempMinorSrc  	= 'HO';

							$sql_soap		=  "INSERT INTO MAS_APPLICATION SET 
														tempAddrNo 		 	= '$tempLifeAddrNo',
														tempAgentLicense  = '$tempAgentLicense',
														tempAmpNm  		= '$tempLifeAmphur',
														tempBeneRate  		= '100',
														tempBeneRate2  	= '',
														tempBeneRate3  	= '',
														tempBeneRate4  	= '',
														tempBeneRate5  	= '',
														tempBld  				= '$tempLifeBuilding',
														tempChwNm  		= '$tempLifeChangewat',
														tempCommGrp  		= '$tempCommGrp',
														tempDayendDt  		= '$tempDayendDt',
														tempEffectDt  		= '$tempEffectDt',
														tempEmail  			= '$tempEmail',
														tempExpiryDt  		= '$tempExpiryDt',
														tempFirstBeneNm  	= '$tempFirstBeneNm',
														tempFirstBeneNm2 = '',
														tempFirstBeneNm3 = '',
														tempFirstBeneNm4 = '',
														tempFirstBeneNm5 = '',
														tempFirstLifeNm  	= '$tempFirstLifeNm',
														tempFirstNm  		= '$tempFirstLifeNm',
														tempFormula  		= '$tempFormula',
														tempIcCard  			= '$tempLifeIc',
														tempInformer    		= '$tempInformer',
														tempIntmId  			= '$tempIntmId',
														tempJourneyDesc  	= '-',
														tempLastBeneNm  	= '$tempLastBeneNm',
														tempLastBeneNm2  = '',
														tempLastBeneNm3  = '',
														tempLastBeneNm4  = '',
														tempLastBeneNm5  = '',
														tempLastLifeNm  	= '$tempLastLifeNm',
														tempLastNm  			= '$tempLastLifeNm',
														tempLifeAddrNo  	= '$tempLifeAddrNo',
														tempLifeAmphur  	= '$tempLifeAmphur',
														tempLifeBirth  		= '$tempLifeBirth',
														tempLifeBuilding 	= '$tempLifeBuilding',
														tempLifeChangewat  = '$tempLifeChangewat',
														tempLifeIc  			= '$tempLifeIc',
														tempLifeMobile  		= '$tempLifeMobile',
														tempLifeMoo  		= '',
														tempLifePostcode  	= '$tempLifePostcode',
														tempLifeRoad  		= '',
														tempLifeSoi  			= '',
														tempLifeTambol  	= '$tempLifeTambol',
														tempMainClass  		= '$tempMainClass',
														tempMajorSrc  		= '$tempMajorSrc',
														tempMinorSrc  		= '$tempMinorSrc',
														tempMoo  				= '',
														tempNoPerson  		= '1',
														tempNumberPolicy  = '1',
														tempOccDes  			= '',
														tempOccLevel  		= '',
														tempOrderNo  		= '',
														tempPackage  		= '$tempFormula',
														tempPaymentType  = 'Y',
														tempPostcode  		= '$tempLifePostcode',
														tempPremium  		= '$tempPremium',
														tempQuotationNo  	= '$policy_id',
														tempRefNo1  			= '$receipt_id',
														tempRefNo2  			= '$receipt_id',
														tempRefNo3    		= '$tempDayendDt  ',
														tempRelation  		= '$tempRelation',
														tempRelation2  		= '',
														tempRelation3  		= '',
														tempRelation4  		= '',
														tempRelation5  		= '',
														tempRoad  			= '',
														tempSoi  				= '',
														tempSubclass  		= '$policy_type',
														tempSumInsured  	= '$tempSumInsured',
														tempTelNo  			= '$tempLifeMobile',
														tempThmNm  		= '$tempLifeTambol',
														tempTitleBeneNm  	= '$tempTitleBeneNm',
														tempTitleBeneNm2  = '',
														tempTitleBeneNm3  = '',
														tempTitleBeneNm4  = '',
														tempTitleBeneNm5  = '',
														tempTitleLifeNm  	= '$tempTitleLifeNm',
														tempTitleNm  		= '$tempTitleLifeNm',
														tempYear  				= '$tempYear',
														tempStatus  			= '$tempStatus',
														tempTranfer  		= '$tempDayendDt',
														POLICY_ID  			= '$policy_id',
														RECEIPT_ID  			= '$receipt_id',
														POLICY_COMDATE 	= '$COMDATE',
														POLICY_EXPDATE	= '$EXPDATE',
														POLICY_TRANDATE	= '$tempDayendDt',
														tempBeneIc			= '$tempBeneIc' ";
														mysql_query($sql_soap) or die (mysql_error());		
							//-------- / INSERT MAS POLICY ORDER ------------//	

							$sql = "SELECT SENT_NO FROM TXN_LOG_SOAP";
							$sql .= " WHERE POLICY_ID = '" .$policy_id."'";
							$sql .= " ORDER BY SENT_NO DESC";
							$result = mysql_query($sql);
							
							if ($rs = mysql_fetch_array($result)) {
								$sent_no = strval(intval($rs["SENT_NO"]) + 1);
							} else {
								$sent_no = '1';
							}
							
							$return  = array(
								"tempStatus"=>"SUCCESS",
								"Message"=>array("POLICY_NO"=>$policy_id,
																"RECEIPT_ID"=>$receipt_id,
																"INVOICE_ID"=>"",
															));
							$res		= json_encode($return);
							
							$message =  "Success:".$policy_id.":".$receipt_id.":";
			
							$sql = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE,AGENT_ID) VALUES";
							$sql .= " ('".$policy_id."', ". $sent_no .", 'Frank_PA', now()";
							$sql .= ", '".getenv("REMOTE_ADDR")."', '1', '" .$message ."', '" . $snote ."','')";
							mysql_unbuffered_query($sql);																				
							mysql_free_result($result);	
												
							$url = 'https://www.siamcityinsurance.com:9843/schedule/SiamPA_EPolicy.php'; // กำหนด URl ของ API
							
							$user_api	= 'Fr@nk2Gi';
							$password_api	= 'FwdGi2@20';
							$policy		= array("policy_id"=>$policy_id);
							$request	= json_encode($policy);
						  
							$ch = curl_init(); // เริ่มต้นใช้งาน cURL
							  
							curl_setopt($ch, CURLOPT_URL, $url); 
							curl_setopt($ch, CURLOPT_POST, 1); 
							curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
							curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
							curl_setopt($ch, CURLOPT_USERPWD, $user_api.":".$password_api);
							  
							$response = curl_exec($ch); 
							curl_close($ch);

					   }
				}
			}
		} else {
		$return  = array(
		"tempStatus"=>"FAIL",
		"Message"=>array("tempOrderNo"=>"",
										"ErrorCode"=>"1SYS00",
										"ErrorMessage"=>"Unauthorized user & password incorrect",
									));
		$res		= json_encode($return);
	}
echo $res;
return;
?>