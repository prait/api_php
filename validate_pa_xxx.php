<?php

	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST");
	header('Content-Type: application/json');
	//header('WWW-Authenticate: Basic realm="https://www.siamcityinsurance.com",charset="UTF-8" '); 
	//header("HTTP/1.0 401 Unauthorized");
		
	if (trim($_SERVER['PHP_AUTH_USER'])=="Fr@nk2Gi" and trim($_SERVER['PHP_AUTH_PW']) == "FwdGi2@20") {

		include_once("../include/misc.php");
		$conn = connect_db();

			$data = json_decode(file_get_contents('php://input'), true);
				
			foreach ($data as $pol) {
				$tempOrderNo			=	$pol["tempOrderNo"];
				$tempAgentLicense		=	$pol["tempAgentLicense"]; 
				$tempIntmId				=	$pol["tempIntmId"]; 
				$tempTitleLifeNm		= 	$pol["tempTitleLifeNm"]; 
				$tempFirstLifeNm		=	$pol["tempFirstLifeNm"]; 
				$tempLastLifeNm		=	$pol["tempLastLifeNm"]; 
				$tempLifeIc				=	$pol["tempLifeIc"]; 
				$tempLifeBirth			=	$pol["tempLifeBirth"]; 
				$tempLifeAddrNo		=	$pol["tempLifeAddrNo"];
				$tempLifeBuilding 		=	$pol["tempLifeBuilding"]; 
				$tempLifeTambol		= 	$pol["tempLifeTambol"]; 
				$tempLifeAmphur		=	$pol["tempLifeAmphur"]; 
				$tempLifeChangewat	=	$pol["tempLifeChangewat"]; 
				$tempLifePostcode		=	$pol["tempLifePostcode"]; 
				$tempLifeMobile			=	$pol["tempLifeMobile"]; 
				$tempEmail				=	$pol["tempEmail"]; 
				$tempFormula			=	$pol["tempFormula"]; 
				//$tempNoPerson		=	$pol["tempNoPerson"]; 
				$tempEffectDt			=	$pol["tempEffectDt"]; 
				$tempExpiryDt			=	$pol["tempExpiryDt"]; 
				$tempPremium			= 	$pol["tempPremium"]; 
				//$tempQuotationNo	=  $pol["tempQuotationNo"]; 
				//$tempRefNo1			=  $pol["tempRefNo1"]; 
				//$tempRefNo2			=  $pol["tempRefNo2"]; 
				//$tempSubclass		=	$pol["tempSubclass"]; 
				//$tempSumInsured	=  $pol["tempSumInsured"]; 
				$tempTitleBeneNm		=  $pol["tempTitleBeneNm"]; 
				$tempFirstBeneNm		=  $pol["tempFirstBeneNm"]; 
				$tempLastBeneNm		=  $pol["tempLastBeneNm"]; 
				$tempRelation			=  $pol["tempRelation"]; 
				$tempBeneIc				= 	$pol["tempBeneIc"]; 
				//$tempBeneRate		=  $pol["tempBeneRate"];
				$tempStatus				=  $pol["tempStatus"];

				$snote .= 	"tempOrderNo = ".$tempOrderNo."\n";
				$snote .= 	"tempAgentLicense = ".$tempAgentLicense."\n";
				$snote .= 	"tempIntmId = ".$tempIntmId."\n";
				$snote .= 	"tempTitleLifeNm = ".$tempTitleLifeNm."\n";
				$snote .= 	"tempFirstLifeNm = ".$tempFirstLifeNm."\n";
				$snote .= 	"tempLastLifeNm = ".$tempLastLifeNm."\n";
				$snote .= 	"tempLifeIc = ".$tempLifeIc."\n";
				$snote .= 	"tempLifeBirth = ".$tempLifeBirth."\n";		
				$snote .= 	"tempLifeAddrNo = ".$tempLifeAddrNo."\n";
				$snote .= 	"tempLifeBuilding = ".$tempLifeBuilding."\n";
				$snote .= 	"tempLifeTambol = ".$tempLifeTambol."\n";
				$snote .= 	"tempLifeAmphur = ".$tempLifeAmphur."\n";
				$snote .= 	"tempLifeChangewat = ".$tempLifeChangewat."\n";
				$snote .= 	"tempLifePostcode = ".$tempLifePostcode."\n";
				$snote .= 	"tempLifeMobile = ".$tempLifeMobile."\n";
				$snote .= 	"tempEmail = ".$tempEmail."\n";
				$snote .= 	"tempFormula = ".$tempFormula."\n";	
				$snote .= 	"tempEffectDt = ".$tempEffectDt."\n";	
				$snote .= 	"tempExpiryDt = ".$tempExpiryDt."\n";	
				$snote .= 	"tempPremium = ".$tempPremium."\n";	
				$snote .= 	"tempSumInsured = ".$tempSumInsured."\n";	
				$snote .= 	"tempTitleBeneNm = ".$tempTitleBeneNm."\n";	
				$snote .= 	"tempFirstBeneNm = ".$tempFirstBeneNm."\n";	
				$snote .= 	"tempLastBeneNm = ".$tempLastBeneNm."\n";	
				$snote .= 	"tempRelation = ".$tempRelation."\n";	
				$snote .= 	"tempBeneIc = ".$tempBeneIc."\n";	
				$snote .= 	"tempStatus = ".$tempStatus."\n";	
					
				$COMDATE 	= $tempEffectDt;
				$COMDATE	= substr($tempEffectDt, -4)."-".substr($tempEffectDt,3,2)."-".substr($tempEffectDt,0,2);
				$EXPDATE 	= $tempExpiryDt;
				$EXPDATE 	= substr($tempExpiryDt, -4)."-".substr($tempExpiryDt,3,2)."-".substr($tempExpiryDt,0,2);
				
				$date_regex = '/^(0[1-9]|[12][0-9]|3[01])[\-\/.](0[1-9]|1[012])[\-\/.](19|20)\d\d$/';	
	
				if ((!preg_match($date_regex,$tempEffectDt)) || (!preg_match($date_regex,$tempExpiryDt))) {
					$return  = array(
						"tempStatus"=>"0",
						"MESSAGE"=>"Must be format DD/MM/YYYY !!!",
					);
					$res		= json_encode($return);
					
				} else if(strtotime($COMDATE) < strtotime(date("Y-m-d"))) {
					$return  = array(
						"tempStatus"=>"0",
						"MESSAGE"=>"Can not send ceffectDt back date !!!",
					);
					$res		= json_encode($return);

				} else if(strtotime($COMDATE) >= strtotime($EXPDATE)) {
					$return  = array(
						"tempStatus"=>"0",
						"MESSAGE"=>"ceffectDt must less than cexpiryDt !!!",
					);
					$res		= json_encode($return);
				
				} else if(($tempFormula <> "PAO01") && ($tempFormula <> "PAO02") && ($tempFormula <> "PAO03") && ($tempFormula <> "PAO04") && ($tempFormula <> "PAO05") && ($tempFormula <> "PAO06") && ($tempFormula <> "PAO07") && ($tempFormula <> "PAO08") && ($tempFormula <> "UNS01") && ($tempFormula <> "UNS02") && ($tempFormula <> "UNS03") && ($tempFormula <> "UNS04") && ($tempFormula <> "UNS05") && ($tempFormula <> "UNS06")){
					$return  = array(
						"tempStatus"=>"0",
						"MESSAGE"=>"tempFormula incorrect !!",
					);
					$res		= json_encode($return);

				} else {

						$sql = "SELECT * FROM MAS_APPLICATION";
						$sql .= " WHERE tempIcCard = '" .$tempIcCard ."'";
						$sql .= " AND tempSubclass = 'PAO'";
						$sql .= " AND POLICY_EXPDATE > '" .$comdate ."'";
						$sql .= " AND tempStatus <> 0 ";
						$result = mysql_query($sql);
						$num   = mysql_num_rows($result);	
						if($num > 0 ) {	
						//if ($rs = mysql_fetch_array($result)) {
							if(($tempFormula <> "PAO01") && ($tempFormula <> "PAO02") && ($tempFormula <> "UNS01") && ($tempFormula <> "UNS02")) {
								$return  = array(
									"tempStatus"=>"0",
									"MESSAGE"=>"tempIcCard " .$tempIcCard. " except Plan 1 & Plan 2  only 1 policies/person)",
									//"MESSAGE"=>"tempIcCard " .$tempIcCard. " is duplicate with policy ".$policy_temp."  (Expire date: ".$expdate_temp. ")",
									);
								$res		= json_encode($return);
							} else if($num >= '2') {
									$return  = array(
									"tempStatus"=>"0",
									"MESSAGE"=>"tempIcCard " .$tempIcCard. " Plan 1 & Plan 2  max 2 policies/person)",
									//"MESSAGE"=>"tempIcCard " .$tempIcCard. " is duplicate with policy ".$policy_temp."  (Expire date: ".$expdate_temp. ")",
									);
								$res		= json_encode($return);
							}
							
							$sql = "SELECT SENT_NO FROM TXN_LOG_SOAP";
							$sql .= " WHERE POLICY_ID = '".$policy_temp."'";
							$sql .= " ORDER BY SENT_NO DESC";
							$result = mysql_query($sql);
							
							if ($rs = mysql_fetch_array($result)) {
								$sent_no = strval(intval($rs["SENT_NO"]) + 1);
							} else {
								$sent_no = '1';
							}
					
							$sql = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE,AGENT_ID) VALUES";
							$sql .= " ('".$tempOrderNo."', ". $sent_no .", 'Frank_PA', now()";
							$sql .= ", '".getenv("REMOTE_ADDR")."', '0', 'Duplicate', '" . $snote ."','')";
							mysql_unbuffered_query($sql);

						} else {
							
							$tempStatus = '1';
	
							$sql = "SELECT SENT_NO FROM TXN_LOG_SOAP";
							$sql .= " WHERE POLICY_ID = '" .$tempOrderNo."'";
							$sql .= " ORDER BY SENT_NO DESC";
							$result = mysql_query($sql);
							
							if ($rs = mysql_fetch_array($result)) {
								$sent_no = strval(intval($rs["SENT_NO"]) + 1);
							} else {
								$sent_no = '1';
							}
							
							$return  = array(
											"tempStatus"=>"1",
											"MESSAGE"=>"successful",
											);
							$res		= json_encode($return);
							
							$message =  "Success:".$tempOrderNo.":".$tempStatus.":";
			
							$sql = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE,AGENT_ID) VALUES";
							$sql .= " ('".$tempOrderNo."', ". $sent_no .", 'Frank_PA', now()";
							$sql .= ", '".getenv("REMOTE_ADDR")."', '1', '" .$message ."', '" . $snote ."','')";
							mysql_unbuffered_query($sql);																				
							mysql_free_result($result);	
												

					   }
				}
			}
		} else {
			$return  = array(
							"tempStatus"=>"0",
							"MESSAGE"=>"user & password incorrect",
							);
				
			$res		= json_encode($return);
		}
	echo $res." ".$policy;
	return;
?>